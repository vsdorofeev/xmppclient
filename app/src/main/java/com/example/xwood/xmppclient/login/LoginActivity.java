package com.example.xwood.xmppclient.login;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.xwood.xmppclient.BaseActivity;
import com.example.xwood.xmppclient.R;
import com.example.xwood.xmppclient.chat.ChatActivity;
import com.example.xwood.xmppclient.network.LoginTask;
import com.example.xwood.xmppclient.network.XmppServerConnection;
import com.example.xwood.xmppclient.settings.SettingsActivity;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;

public class LoginActivity extends BaseActivity implements LoginCallback {

    private Button mBtnLogin;
    private EditText mEtLogin;
    private EditText mEtPassword;
    private TextInputLayout mTilLogin;
    private TextInputLayout mTilPassword;
    private ProgressBar mProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUi();
        setListeners();
    }


    private void initUi(){
        mBtnLogin = (Button) findViewById(R.id.btn_login);
        mEtLogin = (EditText) findViewById(R.id.et_login);
        mEtPassword = (EditText) findViewById(R.id.et_password);
        mTilLogin = (TextInputLayout) findViewById(R.id.til_login);
        mTilPassword = (TextInputLayout) findViewById(R.id.til_password);

        mProgress = (ProgressBar) findViewById(R.id.progress_bar);
        mProgress.setVisibility(View.GONE);

        mEtLogin.setText("vdclient@jabber.ru");
        mEtPassword.setText("Qfg2kfclient");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            Intent intent = new Intent(LoginActivity.this, SettingsActivity.class);
            startActivity(intent);
        } else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    private void setListeners() {
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogAndPassValid()){
                    attemptToLogin();
                }
            }
        });
    }


    private void attemptToLogin(){
        String login = mEtLogin.getText().toString();
        String password = mEtPassword.getText().toString();
        XMPPTCPConnection con = XmppServerConnection.getInstance().getConnection();
        LoginTask loginTask = new LoginTask(con, mProgress, login, password, this);
        loginTask.execute();
    }


    private boolean isLogAndPassValid(){
        String login = mEtLogin.getText().toString();
        String password = mEtPassword.getText().toString();

        if (login.length() == 0 || password.length() == 0){
            Toast.makeText(this, R.string.error_empty_login_or_password, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!login.contains("@")){
            Toast.makeText(this, R.string.error_invalid_login, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public void onSuccess(XMPPTCPConnection connection) {
        startActivity(new Intent(LoginActivity.this, ChatActivity.class));
    }


    @Override
    public void onFailure() {
        Toast.makeText(this, R.string.error_on_failure_logincallback, Toast.LENGTH_SHORT).show();
    }
}
