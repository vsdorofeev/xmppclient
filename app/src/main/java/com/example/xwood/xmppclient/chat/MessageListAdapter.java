package com.example.xwood.xmppclient.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xwood.xmppclient.Config;
import com.example.xwood.xmppclient.R;

import org.jivesoftware.smack.packet.Message;

import java.util.List;

/**
 * Created by xwood on 04.09.17.
 */

public class MessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_MESSAGE_SENDED = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private Context mContext;
    private List<Message> mMessageList;

    public MessageListAdapter(Context mContext, List<Message> mMessageList) {
        this.mContext = mContext;
        this.mMessageList = mMessageList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENDED){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_sended_message, parent, false);
            return new SendedMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_received_message, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Message message = mMessageList.get(position);

        switch (holder.getItemViewType()){
            case VIEW_TYPE_MESSAGE_SENDED:
                ((SendedMessageHolder) holder).bind(message);
                break;

            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
        }
    }

    @Override
    public int getItemViewType(int position) {
        Message message = mMessageList.get(position);
        String to = message.getTo().toString();

        if (to.contains(Config.RESOURCE + Config.DOMAIN)){
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }

        return VIEW_TYPE_MESSAGE_SENDED;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    public void addMessage(Message msg){
        mMessageList.add(msg);
        notifyDataSetChanged();
    }
}
