package com.example.xwood.xmppclient.chat;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.xwood.xmppclient.R;

import org.jivesoftware.smack.packet.Message;

import java.text.SimpleDateFormat;

/**
 * Created by xwood on 04.09.17.
 */

public class SendedMessageHolder extends RecyclerView.ViewHolder {

    private TextView mTvMessageBody;
    private TextView mTvMessageTime;


    SendedMessageHolder(View itemView) {
        super(itemView);

        mTvMessageBody = (TextView) itemView.findViewById(R.id.tv_message_body);
        mTvMessageTime = (TextView) itemView.findViewById(R.id.tv_message_time);
    }

    void bind(Message message) {
        mTvMessageBody.setText(message.getBody());

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String msgDate = sdf.format(new java.util.Date());
        mTvMessageTime.setText(msgDate);
    }

}
