package com.example.xwood.xmppclient;

/**
 * Created by xwood on 04.09.17.
 */

public class Config {

    public static final String RESOURCE = "vdclient@";
    public static final String DOMAIN = "jabber.ru";
    public static final int PORT = 5222;

}
