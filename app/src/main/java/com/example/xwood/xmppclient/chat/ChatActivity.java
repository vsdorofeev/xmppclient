package com.example.xwood.xmppclient.chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.xwood.xmppclient.BaseActivity;
import com.example.xwood.xmppclient.Config;
import com.example.xwood.xmppclient.R;
import com.example.xwood.xmppclient.network.XmppServerConnection;
import com.example.xwood.xmppclient.settings.SettingsActivity;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends BaseActivity {

    private EditText mEtMessage;
    private Button mBtnSend;
    private RecyclerView mRvMessageList;
    private ChatManager mChatManager;
    private Chat mChat;
    private EntityBareJid mRecipientJid;
    private MessageListAdapter mAdapter;
    private Handler onMessageReceivHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        initUi();
        initChat();
        setListeners();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            Intent intent = new Intent(ChatActivity.this, SettingsActivity.class);
            startActivity(intent);
        } else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUi() {
        mEtMessage = (EditText) findViewById(R.id.et_message);
        mBtnSend = (Button) findViewById(R.id.btn_send_message);
        mRvMessageList = (RecyclerView) findViewById(R.id.rv_message_list);

        List<Message> list = new ArrayList<>();
        mAdapter = new MessageListAdapter(this, list);
        mRvMessageList.setAdapter(mAdapter);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        mRvMessageList.setLayoutManager(manager);

        onMessageReceivHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(android.os.Message msg) {
                mAdapter.addMessage((Message) msg.obj);
                return true;
            }
        });

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String botJid = sharedPref.getString("botjid", "");
        Log.d("TAG", botJid);

        try {
            mRecipientJid = JidCreate.entityBareFrom(botJid);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }
    }


    private void sendMessage(){
        String msgBody = mEtMessage.getText().toString();
        if (msgBody.length() > 0){
            try {
                Message message = new Message(mRecipientJid, msgBody);
                mAdapter.addMessage(message);
                mChat.send(message);

            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private void initChat(){
        XMPPTCPConnection con = XmppServerConnection.getInstance().getConnection();
        if (con.isConnected()){
            mChatManager = ChatManager.getInstanceFor(con);
            mChat = mChatManager.chatWith(mRecipientJid);
        }
    }


    private void setListeners() {
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        mChatManager.addIncomingListener(new IncomingChatMessageListener() {
            @Override
            public void newIncomingMessage(EntityBareJid from, Message message, Chat chat) {
                android.os.Message msg = new android.os.Message();
                msg.obj = message;
                onMessageReceivHandler.sendMessage(msg);
            }
        });

    }

}
