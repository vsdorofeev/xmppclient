package com.example.xwood.xmppclient.login;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;

/**
 * Created by xwood on 05.09.17.
 */

public interface LoginCallback {

    void onSuccess(XMPPTCPConnection connection);
    void onFailure();

}
