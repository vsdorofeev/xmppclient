package com.example.xwood.xmppclient.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.example.xwood.xmppclient.R;

import org.jivesoftware.smack.packet.Message;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by xwood on 04.09.17.
 */

public class ReceivedMessageHolder extends RecyclerView.ViewHolder {

    private TextView mTvMessageName;
    private TextView mTvMessageBody;
    private TextView mTvMessageTime;

    public ReceivedMessageHolder(View itemView) {
        super(itemView);

        mTvMessageName = (TextView) itemView.findViewById(R.id.tv_message_name);
        mTvMessageBody = (TextView) itemView.findViewById(R.id.tv_message_body);
        mTvMessageTime = (TextView) itemView.findViewById(R.id.tv_message_time);
    }

    void bind(Message message) {
        mTvMessageBody.setText(message.getBody());

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String msgDate = sdf.format(new java.util.Date());
        mTvMessageTime.setText(msgDate);

        mTvMessageName.setText(message.getFrom());
    }

}
