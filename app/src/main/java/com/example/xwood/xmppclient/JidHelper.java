package com.example.xwood.xmppclient;

import org.jxmpp.jid.EntityBareJid;

/**
 * Created by xwood on 05.09.17.
 */

public class JidHelper {

    public static JidHelper getInstance(){
        return InstanceHolder.INSTANCE;
    }

    public EntityBareJid getBotJid(){
        return null;
    }

    private static class InstanceHolder{
        public static JidHelper INSTANCE;
    }
}
