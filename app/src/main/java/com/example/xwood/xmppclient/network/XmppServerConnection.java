package com.example.xwood.xmppclient.network;

import android.content.Context;
import android.widget.ProgressBar;

import com.example.xwood.xmppclient.Config;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jxmpp.stringprep.XmppStringprepException;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by xwood on 04.09.17.
 */

public class XmppServerConnection {

    private String mLogin;
    private String mPassword;
    private XmppServerConnection INSTANCE;
    private static XMPPTCPConnection mConnection;


    public static XmppServerConnection getInstance() {
        return ConnectionHolder.INSTANCE;
    }

    private static void initializeConnection(){
        XMPPTCPConnectionConfiguration config = null;


        try {
            InetAddress address = InetAddress.getByName("95.108.194.209");
            config = XMPPTCPConnectionConfiguration.builder()
                    .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                    .setDebuggerEnabled(true)
                    .setXmppDomain("jabber.ru")
                    .setHost("www.jabber.ru")
                    .setHostAddress(address)
                    .setPort(Config.PORT)
                    .build();
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        if (config != null){
            mConnection = new XMPPTCPConnection(config);
        }
    }

    public XMPPTCPConnection getConnection(){
        if (mConnection == null){
            initializeConnection();
        }

        return mConnection;
    }

    public static class ConnectionHolder {
        public static final XmppServerConnection INSTANCE = new XmppServerConnection();
    }
}


