package com.example.xwood.xmppclient.network;

import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.xwood.xmppclient.Config;
import com.example.xwood.xmppclient.login.LoginCallback;

import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;

/**
 * Created by xwood on 05.09.17.
 */

public class LoginTask extends AsyncTask<Void, Void, XMPPTCPConnection> {

    private XMPPTCPConnection mConnection;
    private ProgressBar mProgress;
    private String mLogin;
    private String mPassword;
    private LoginCallback mCallback;

    public LoginTask(XMPPTCPConnection connection, ProgressBar progress,
                     String login, String password, LoginCallback callback) {
        this.mConnection = connection;
        this.mProgress = progress;
        this.mLogin = login;
        this.mPassword = password;
        this.mCallback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mProgress != null) {
            mProgress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected XMPPTCPConnection doInBackground(Void... params) {
        Looper.prepare();
        if (!mConnection.isConnected()){
            try {
                mConnection.connect();

                SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
                SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
                SASLAuthentication.unBlacklistSASLMechanism("PLAIN");

                mConnection.login(mLogin, mPassword);
                Log.d("TAG", "Connection state (inside task): " + mConnection.isConnected());

            } catch (XMPPException e) {
                e.printStackTrace();
                mCallback.onFailure();
            } catch (SmackException e) {
                e.printStackTrace();
                mCallback.onFailure();
            } catch (IOException e) {
                e.printStackTrace();
                mCallback.onFailure();
            } catch (InterruptedException e) {
                e.printStackTrace();
                mCallback.onFailure();
            }
        }

        return mConnection;
    }

    @Override
    protected void onPostExecute(XMPPTCPConnection connection) {
        super.onPostExecute(connection);
        if (mProgress != null) {
            mProgress.setVisibility(View.GONE);
        }
        mCallback.onSuccess(connection);
    }
}
